# GoNet

Training architecture for playing the game of go. Project carried out within the context of the Deep Learning course of the master IASD.

## How to use ?

### Download the dataset
```
wget https://www.lamsade.dauphine.fr/~cazenave/games.1000000.data.zip
unzip games.1000000.data.zip
```

### Compile the C files

```
./compile.sh
```

### Train the model

```
python3 golois.py
```

Or

```
nohup python3 golois.py > output.txt 2> output.err < /dev/null &
```