import numpy as np
import tensorflow.keras as keras
from tensorflow.keras.utils import Sequence

import golois


planes = 31
moves = 361


class DataLoader(Sequence):

    def __init__(self, batch_size=128, nb_batch=80):
        self.batch_size = batch_size
        self.nb_batch = nb_batch
        self.N = self.batch_size * self.nb_batch
        self.epoch_index = 0
        input_data = np.random.randint(2, size=(self.batch_size, 19, 19, planes))
        self.input_data = input_data.astype('float32')

        policy = np.random.randint(moves, size=(self.batch_size,))
        self.policy = keras.utils.to_categorical(policy, num_classes=moves)

        value = np.random.randint(2, size=(self.batch_size,))
        self.value = value.astype('float32')

        end = np.random.randint(2, size=(self.batch_size, 19, 19, 2))
        self.end = end.astype('float32')

        groups = np.zeros((self.batch_size, 19, 19, 1))
        self.groups = groups.astype('float32')

    def __len__(self):
        return self.nb_batch

    def __getitem__(self, index):
        golois.getBatch(
            self.input_data,
            self.policy,
            self.value,
            self.end,
            self.groups,
            index * self.batch_size + self.epoch_index * self.N
        )
        return self.input_data, {'policy': self.policy, 'value': self.value}

    def on_epoch_end(self):
        self.epoch_index += 1


class ValidationLoader(Sequence):

    def __init__(self, size_validation=10000):
        input_data = np.random.randint(2, size=(size_validation, 19, 19, planes))
        self.input_data = input_data.astype('float32')

        policy = np.random.randint(moves, size=(size_validation,))
        self.policy = keras.utils.to_categorical(policy, num_classes=moves)

        value = np.random.randint(2, size=(size_validation,))
        self.value = value.astype('float32')

        end = np.random.randint(2, size=(size_validation, 19, 19, 2))
        self.end = end.astype('float32')

        golois.getValidation(self.input_data, self.policy, self.value, self.end)

    def __len__(self):
        return 1

    def __getitem__(self, index):
        return self.input_data, {'policy': self.policy, 'value': self.value}
