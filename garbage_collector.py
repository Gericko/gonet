from keras.callbacks import Callback
import gc


class GarbageCollector(Callback):

    def __init__(self, period=5):
        super(GarbageCollector, self).__init__()
        self.period = period

    def on_epoch_end(self, epoch, logs=None):
        if epoch % self.period == 0:
            gc.collect()
