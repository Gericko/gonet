from mobilenet import get_mobilenet
from visualize import combine_histories, show_history
from cosine_annealing import CosineAnnealingScheduler
from lr_finder import LRFinder
from data_loader import DataLoader, ValidationLoader
from garbage_collector import GarbageCollector


N = 10000
epochs = 150
batch = 128


model = get_mobilenet(depth=11, nb_filters_inside=490, nb_filters_output=84)
model.summary()

validation_sequence = ValidationLoader(size_validation=N)
input_sequence = DataLoader(batch_size=batch, nb_batch=80)

callbacks = [
    GarbageCollector(5),
    CosineAnnealingScheduler(T_max=50, eta_max=2e-4, eta_min=2e-7, verbose=0)
]

history = model.fit(
    x=input_sequence,
    epochs=epochs,
    callbacks=callbacks,
    validation_data=validation_sequence
)

model.save('test.h5')

show_history(history.history)
