import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras import activations

from model_head import ValueHead, PolicyHead


planes = 31


class Bottleneck(layers.Layer):

    def __init__(self, nb_filters_inside=512, nb_filters_output=64):
        self.nb_filters_inside = nb_filters_inside
        self.nb_filters_output = nb_filters_output
        super(Bottleneck, self).__init__()
        self.expansion_layer = layers.Conv2D(
            filters=nb_filters_inside, kernel_size=1, activation=activations.swish, padding='same'
        )
        self.depth_conv_layer = layers.DepthwiseConv2D(kernel_size=3, activation=activations.swish, padding='same')
        self.projection_layer = layers.Conv2D(filters=nb_filters_output, kernel_size=1, padding='same')

    def call(self, inputs, *args, **kwargs):
        x = self.expansion_layer(inputs)
        x = self.depth_conv_layer(x)
        x = self.projection_layer(x)
        x = layers.add([x, inputs])
        x = layers.BatchNormalization()(x)
        return x


def get_summary_mobilenet(depth=18, nb_filters_inside=384, nb_filters_output=64):
    input = keras.Input(shape=(19, 19, planes), name='board')
    x = layers.Conv2D(filters=nb_filters_output, kernel_size=3, activation=activations.swish, padding='same')(input)
    for i in range(depth):
        x = Bottleneck(nb_filters_inside=nb_filters_inside, nb_filters_output=nb_filters_output)(x)
    value_head = ValueHead(name="value")(x)
    policy_head = PolicyHead(name="policy")(x)
    model = keras.Model(inputs=input, outputs=[policy_head, value_head])

    model.summary()


def get_mobilenet(depth=18, nb_filters_inside=384, nb_filters_output=64):
    input = keras.Input(shape=(19, 19, planes), name='board')
    x = layers.Conv2D(filters=nb_filters_output, kernel_size=3, activation=activations.swish, padding='same')(input)
    for i in range(depth):
        x1 = layers.Conv2D(filters=nb_filters_inside, kernel_size=1, activation=activations.swish, padding='same')(x)
        x1 = layers.DepthwiseConv2D(kernel_size=3, activation=activations.swish, padding='same')(x1)
        x1 = layers.Conv2D(filters=nb_filters_output, kernel_size=1, padding='same')(x1)
        x = layers.add([x1, x])
        x = layers.BatchNormalization()(x)
    policy_head = layers.Conv2D(
        1, 1, activation='relu', padding='same', use_bias=False, kernel_regularizer=regularizers.l2(0.0001)
    )(x)
    policy_head = layers.Flatten()(policy_head)
    policy_head = layers.Activation('softmax', name='policy')(policy_head)
    value_head = layers.GlobalAveragePooling2D()(x)
    value_head = layers.Dense(50, activation='relu', kernel_regularizer=regularizers.l2(0.0001))(value_head)
    value_head = layers.Dense(
        1, activation='sigmoid', name='value', kernel_regularizer=regularizers.l2(0.0001)
    )(value_head)
    model = keras.Model(inputs=input, outputs=[policy_head, value_head])

    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=2e-4),
        loss={'policy': 'categorical_crossentropy', 'value': 'binary_crossentropy'},
        loss_weights={'policy': 1.0, 'value': 1.0},
        metrics={'policy': 'categorical_accuracy', 'value': 'mse'}
    )

    return model


if __name__ == "__main__":
    get_summary_mobilenet(depth=11, nb_filters_inside=490, nb_filters_output=84)
