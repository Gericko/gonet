from tensorflow.keras import layers
from tensorflow.keras import regularizers


class PolicyHead(layers.Layer):

    def __init__(self, name=None):
        super(PolicyHead, self).__init__(name=name)
        self.conv_layer = layers.Conv2D(
            1, 1, activation='relu', padding='same', use_bias=False, kernel_regularizer=regularizers.l2(0.0001)
        )
        self.activ_layer = layers.Activation('softmax', name='policy')

    def call(self, inputs, **kwargs):
        x = self.conv_layer(inputs)
        x = layers.Flatten()(x)
        x = self.activ_layer(x)
        return x


class ValueHead(layers.Layer):

    def __init__(self, name=None):
        super(ValueHead, self).__init__(name=name)
        self.dense_layer_1 = layers.Dense(50, activation='relu', kernel_regularizer=regularizers.l2(0.0001))
        self.dense_layer_2 = layers.Dense(
            1, activation='sigmoid', name='value', kernel_regularizer=regularizers.l2(0.0001)
        )

    def call(self, inputs, **kwargs):
        x = layers.GlobalAveragePooling2D()(inputs)
        x = self.dense_layer_1(x)
        x = self.dense_layer_2(x)
        return x
