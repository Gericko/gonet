from mobilenet import get_mobilenet
from lr_finder import LRFinder
from data_loader import DataLoader, ValidationLoader


model = get_mobilenet(11, 490, 84)
lr_finder = LRFinder(min_lr=1e-6, max_lr=1e-2, steps_per_epoch=1, epochs=320)
validation_sequence = ValidationLoader(size_validation=1)
input_sequence = DataLoader(batch_size=128, nb_batch=1)

model.summary()

model.fit(x=input_sequence, epochs=320, callbacks=[lr_finder])

lr_finder.plot_loss("mobilenet_wide_loss_lr_graph.png")
