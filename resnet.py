import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers


planes = 31


class InputBlock(layers.Layer):

    def __init__(self, filters=64, size_1=5, size_2=1):
        self.filters = filters
        self.size_1 = size_1
        self.size_2 = size_2
        super(InputBlock, self).__init__()
        self.convolution_layer_1 = layers.Conv2D(self.filters, self.size_1, padding='same')
        self.convolution_layer_2 = layers.Conv2D(self.filters, self.size_2, padding='same')

    def get_config(self):
        config = super().get_config()
        config.update({"filters": self.filters, "size_1": self.size_1, "size_2": self.size_2})
        return config

    def call(self, inputs):
        x1 = self.convolution_layer_1(inputs)
        x2 = self.convolution_layer_2(inputs)
        x = layers.add([x1, x2])
        x = layers.ReLU()(x)
        return x


class ResNetBlock(layers.Layer):

    def __init__(self, filters=64, size=3):
        self.filters = filters
        self.size = size
        super(ResNetBlock, self).__init__()
        self.convlayer_1 = layers.Conv2D(self.filters, self.size, activation='relu', padding='same')
        self.convlayer_2 = layers.Conv2D(self.filters, self.size, padding='same')
        self.normlayer = layers.BatchNormalization()

    def get_config(self):
        config = super().get_config()
        config.update({"filters": self.filters, "size": self.size})
        return config

    def call(self, inputs):
        x1 = self.convlayer_1(inputs)
        x1 = self.convlayer_2(x1)
        x = layers.add([x1, inputs])
        x = layers.ReLU()(x)
        x = self.normlayer(x)
        return x


class PolicyHead(layers.Layer):

    def __init__(self, name=None):
        super(PolicyHead, self).__init__(name=name)
        self.convlayer = layers.Conv2D(
            1, 1, activation='relu', padding='same', use_bias=False, kernel_regularizer=regularizers.l2(0.0001)
        )
        self.activlayer = layers.Activation('softmax', name='policy')

    def call(self, inputs):
        x = self.convlayer(inputs)
        x = layers.Flatten()(x)
        x = self.activlayer(x)
        return x


class ValueHead(layers.Layer):

    def __init__(self, name=None):
        super(ValueHead, self).__init__(name=name)
        self.denselayer_1 = layers.Dense(50, activation='relu', kernel_regularizer=regularizers.l2(0.0001))
        self.denselayer_2 = layers.Dense(
            1, activation='sigmoid', name='value', kernel_regularizer=regularizers.l2(0.0001)
        )

    def call(self, inputs):
        x = layers.GlobalAveragePooling2D()(inputs)
        x = self.denselayer_1(x)
        x = self.denselayer_2(x)
        return x


class ResNet(keras.Model):

    def __init__(self, name=None, depth=10, nb_filters=64, size_filters=3):
        super(ResNet, self).__init__(name=name)
        self.depth = depth
        self.nb_filters = nb_filters
        self.size_filters = size_filters
        self.input_block = InputBlock(nb_filters, 5, 1)
        self.residual_blocks = []
        for i in range(depth):
            self.residual_blocks.append(ResNetBlock(self.nb_filters, self.size_filters))
        self.value_block = ValueHead(name="value")
        self.policy_block = PolicyHead(name="policy")

    def call(self, inputs):
        x = self.input_block(inputs)
        for i in range(self.depth):
            x = self.residual_blocks[i](x)
        value = self.value_block(x)
        policy = self.policy_block(x)
        return [value, policy]


def get_summary_resnet():
    input = keras.Input(shape=(19, 19, planes), name='board')
    x = InputBlock(filters=64)(input)
    for i in range(14):
        x = ResNetBlock(filters=64)(x)
    value_head = ValueHead(name="value")(x)
    policy_head = PolicyHead(name="policy")(x)
    model = keras.Model(inputs=input, outputs=[policy_head, value_head])

    model.summary()


if __name__ == "__main__":
    get_summary_resnet()
