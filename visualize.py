import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import numpy


def combine_histories(history_list):
    combined_history = dict()
    for key in history_list[0].history.keys():
        value = []
        for history in history_list:
            value += history.history[key]
        combined_history[key] = value
    return combined_history


def show_history(
        history_dict,
        policy_accuracy_filename="mobilenet_wide_policy_acc.png",
        value_mse_filename="mobilenet_wide_value_mse.png",
        policy_loss_filename="mobilenet_wide_policy_loss.png",
        value_loss_filename="mobilenet_wide_value_loss.png"
):
    # summarize history_dict for policy accuracy
    plt.plot(history_dict['policy_categorical_accuracy'])
    # plt.plot(history_dict.history_dict['val_policy_categorical_accuracy'])
    plt.title('model categorical accuracy for policy')
    plt.ylabel('categorical accuracy')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.savefig(policy_accuracy_filename, bbox_inches='tight')
    plt.close()
    # summarize history_dict for value mse
    plt.plot(history_dict['value_mse'])
    # plt.plot(history_dict.history_dict['val_value mse'])
    plt.title('model mse for value')
    plt.ylabel('mse')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.savefig(value_mse_filename, bbox_inches='tight')
    plt.close()
    # summarize history_dict for policy loss
    plt.plot(history_dict['policy_loss'])
    # plt.plot(history_dict.history_dict['val_policy_loss'])
    plt.title('model loss for policy')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.savefig(policy_loss_filename, bbox_inches='tight')
    plt.close()
    # summarize history_dict for value loss
    plt.plot(history_dict['value_loss'])
    # plt.plot(history_dict.history_dict['val_value_loss'])
    plt.title('model loss for value')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.savefig(value_loss_filename, bbox_inches='tight')
    plt.close()
